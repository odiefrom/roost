# Roost - Cloud Storage Solution

## What is Roost?

Roost was created as a side project simply to allow people upload files to a web server through port 80.  I originally created it for designers out in the field away from the office, as such, there's very little styling, very basic HTML, and the heavy lifting is all serverside with PHP.  Until v2, Javascript wasn't even included, and even now it's use is limited and kept fail safe (if you don't have Javascript on your device, Roost is still 100% usable).

### What Roost Does:
* Roost allows users to create accounts, create projects, and upload files into the project for review later.
* Roost allows users to make individual files public or private, so that only account holders can see certain files, while other files are available for guests to look at also.
* Roost allows guests to deposit files into a general area for collection later - think of a mailbox slot on your front door.

**WARNING**
> Roost comes "as-is", meaning that I am not responsible for what happens if you use it.  This is a free, open source side project, so be mindful of what you're doing.  That said, the only harm that can be reasonably expected from Roost is to the files it stores, so if that worries you, backup regularly.

> Additionally, know that Roost versions are not upgrade safe from one version to another.  They handle operations in drastically different ways.  As such, always use the newest version at time of release (currently v3.3).

## Installation
Installing Roost is relatively simple, following the steps below:
* On BitBucket, navigate to the list of tags, and download the release you'd like to use (v3.3 is the current, most stable, and most user friendly version).
* Copy the roost folder into your web root. (On Linux, you'd move the entire roost/ to /var/www/html/, if that's your webroot).
* Edit install.php, and supply the username and password for your database at the top.  It's ok to use root credentials, as they won't be transmitted or stored.
* Navigate your browser to the install page, ie. http://your-site.com/roost/install.php
* Once it runs successfully, delete install.php from your server.

Congratulations, Roost is installed!

## Miscellaneous Notes
Roost creates a folder inside it's directory called "roostStorage".  This is where Roost's database credentials are stored (roostCred), and where user files are stored.  It will also create a .htaccess file that prohibits external access to this folder.  If you're still able to see the folder by navigating to http://your-site.com/roost/roostStorage then your httpd.conf file (if using Apache) is incorrectly set up to allow .htaccess files.

Roost's development is currently on pause.  Unless a demand for more features becomes apparent, it will hold at v3.3.

### License
Roost uses the MIT License, as can be found in the LICENSE file.  Basically, this means that you can do whatever you'd like with this software on your own liability.  If you do create a derivative software based on Roost, I would *like* credit for the original, with a link pointing back to https://bitbucket.org/odiefrom/roost, but that is at your discretion.

## Who to Contact
Find a bug in Roost?  Need a custom variant of Roost for your business or home?  Want to contribute information or documentation on other installations?  Create an awesome theme skin you want shared?  Contact me (odiefrom) through BitBucket, and I'll respond when I can either through BitBucket or directly through email.