<html>
<head>
<title>Roost - Guest Upload</title>
</head>
<body>

<?php

if(isset($_REQUEST['uploadInProgress']) && $_REQUEST['uploadInProgress'] == "true")
{
    $name=$_REQUEST['uploadName'];
    $clientName=$_REQUEST['clientName'];
    
    $date = getdate();
    $currentDate = strval($date['year']) . '-' . strval($date['mon']) . '-' . strval($date['mday']) . ' ' . strval($date['hours']) . ':' . strval($date['minutes']);
    
    $projectName = $currentDate . " " . $name . " " . $clientName;
    
    $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
    $dbUsername = rtrim(fgets($cred),"\r\n");
    $dbPass = rtrim(fgets($cred),"\r\n");
    fclose($cred);
    
    $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
    $query = "SELECT id FROM users WHERE name='GUEST'";
    $query = $conn->prepare($query);
    $query->execute();
    $query->bind_result($guestID);
    $query->fetch();
    $query->close();
    
    $query = "INSERT INTO projects (user, projectName) VALUES (?,?)";
    $query = $conn->prepare($query);
    $query->bind_param("is", $guestID, $projectName);
    $query->execute();
    $query->close();
    
    $query = "SELECT id FROM projects WHERE user=? AND projectName=?";
    $query = $conn->prepare($query);
    $query->bind_param("is", $guestID, $projectName);
    $query->execute();
    $query->bind_result($projectID);
    $query->fetch();
    $query->close();
    
    mkdir("roostStorage" . DIRECTORY_SEPARATOR . $projectID);
    chmod("roostStorage" . DIRECTORY_SEPARATOR . $projectID, 0755);
    chdir("roostStorage" . DIRECTORY_SEPARATOR . $projectID);

    for($count=0;$count<count($_FILES['uploadedFiles']['tmp_name']);$count++)
    {
        move_uploaded_file($_FILES['uploadedFiles']['tmp_name'][$count], $_FILES['uploadedFiles']['name'][$count]);
        // print("File \"" . $_FILES['uploadedFiles']['name'][$count] . "\" uploaded to: <a href=\"files.php?file=" . urlencode($_FILES['uploadedFiles']['name'][$count]) . "&projectID=" . urlencode($projectID) . "\">HERE</a><br>");
        $query = "INSERT INTO files (projectId, filename, public) VALUES (?,?,'public')";
        $query = $conn->prepare($query);
        $query->bind_param("is",$projectID,$_FILES['uploadedFiles']['name'][$count]);
        $query->execute();
        $query->close();
    }
    
    $query = "SELECT outboundEmail,siteName,siteAddress FROM settings";
    $query = $conn->prepare($query);
    $query->execute();
    $query->bind_result($outboundEmail,$siteName,$siteAddress);
    $query->fetch();
    $query->close();
    
    // Email Notify All Users Code
    $query = "SELECT email from users WHERE name <> 'GUEST'";
    $query = $conn->prepare($query);
    $query->execute();
    $query->bind_result($userEmail);
    $query->store_result();
    $query->fetch();
    $to = $userEmail;
    while($query->fetch()) {
        $to .= ', ' . $userEmail;
    }
    $subject = "Upload to " . $siteName . " has been received";
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: " . $siteName . " Roost <" . $outboundEmail . ">\r\n";
    $headers .= "Subject: " . $subject;
    $message = "
        <html>
        <head><title>Upload to " . $siteName . " has been received</title></head>
        <body>
            <p>A project has been uploaded to Roost.<br>Project Name: " . $projectName . "<br>Client Name: " . $clientName . "</p>
            <p><a href='" . $siteAddress . "/login.php'>Log in</a> to view the Project that has been added.</p>
            <p>Thank you for using Roost!</p>
        </body>
        </html>
    ";
    mail($to,$subject,$message,$headers); // Send mail to all users
    
    // Email Uploader Receipt
    $to = $_REQUEST['clientEmail'];
    $subject = "Upload to " . $siteName . " has been received";
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: " . $siteName . " Roost <" . $outboundEmail . ">\r\n";
    $headers .= "Subject: " . $subject;
    $message = '
        <html>
        <head>
            <title>Upload to ' . $siteName . ' has been received</title>
        </head>
        <body>
            <p>Your upload to Roost has been successful!<br>Here is a list of files and their links for your reference.</p>
            <p><strong>Note: As the users of Roost work with your files, the links may become unavailble.<br>
                This is normal!  Should you need access to these files again, just contact your Point of Contact and ask them to set the files to public.
            </p>
            <p>
            <table border="1">
                <tr><th>File Name</th><th>File Link</th><th><span title="Might not work on some email clients">Direct Download (?)</span></th></tr>';
    $query = "SELECT id,fileName FROM files WHERE projectId=?";
    $query = $conn->prepare($query);
    $query->bind_param('i', $projectID);
    $query->execute();
    $query->bind_result($fileID,$fileName);
    $query->store_result();
    while($query->fetch()) {
        $appending = '<tr><td>' . $fileName . "</td><td><a href='" . $siteAddress . "/files.php?fileID=" . $fileID . "'>View Online</a></td>
            <td><a href='" . $siteAddress . "/files.php?fileID=" . $fileID . "' download='" . $fileName . "'>Direct Download</a></td></tr>";
            $message .= $appending;
    }         
    $message .= '</table>
            </p>
            <p>Thank you for using Roost!</p>
        </body>
        </html>';
    mail($to, $subject, $message, $headers); // Send mail to uploader
    
    // Clean redirect to page with "success" status
    header("Location: guest.php?success=true");
}

if(isset($_REQUEST['success']) && $_REQUEST['success'] == "true") {
    echo "<p>Success!  Files have been properly uploaded.<br>You should be receiving an email notification to confirm receipt.</p>";
}

?>

<h2>Upload File(s) as a Guest</h2>
<p>Use this page to upload file(s) for collection.
<br>
Select the file(s), then click upload.  The page will refresh, and your file(s) will be uploaded.
</p>
<p>Only use AlphaNumeric characters in the fields.</p>
<p>
<form action="guest.php" enctype="multipart/form-data" method="POST">
	<input type="hidden" name="uploadInProgress" value="true"><br>
	<input type="file" multiple="multiple" name="uploadedFiles[]"><br>
	Project Name [Required]: <input type="text" name="uploadName"><br>
	Your Name [Required]: <input type="text" name="clientName"><br>
    Your email [Required]: <input type="text" name="clientEmail"><br>
	<input type="submit" value="Upload"><br>
</form>
</p>

</body>
</html>

