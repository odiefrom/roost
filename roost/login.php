<html>
<head>
<title>Roost - Login</title>
</head>
<body>
<?php

    session_start();
    
    if(isset($_REQUEST['email']) && isset($_REQUEST['password']))
    {
        $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
        $dbUsername = rtrim(fgets($cred),"\r\n");
        $dbPass = rtrim(fgets($cred),"\r\n");
        fclose($cred);
        
        $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
        if($conn->connect_error)
            die("Database failure.");
        
        $query = "SELECT id,password FROM users WHERE email=?";
        $query = $conn->prepare($query);
        $query->bind_param("s",$_REQUEST['email']);
        $query->execute();
        $query->bind_result($id,$password);
        $query->store_result();
        if($query->fetch()) {
            $query->close();
            
            if(password_verify($_REQUEST['password'],$password) {
                $_SESSION['id'] = $id;
                
                //Check if admin
                $query = "SELECT id FROM adminUsers WHERE userId=? AND currentlyAdmin='true'";
                $query = $conn->prepare($query);
                $query->bind_param('i',$_SESSION['id']);
                $query->execute();
                $query->bind_result($adminID);
                $query->store_result();
                if($query->fetch()) {
                    $_SESSION['adminID'] = $adminID;
                }
                else {
                    $_SESSION['adminID'] = -1;
                }
                $query->close();
                
                $conn->close();
                header("Location: projects.php"); // PUNCH IT CHEWIE!
                exit();
            }
            else {
                print("<p>Invalid password.  Please try again.</p>\n");
            }
        }
        $query->close();
        $conn->close();
    }
?>

<p>
Existing user?  Welcome Back to Roost!
<form name="login" action="login.php" method="post">
	Email: <input type="text" name="email"><br>
	Password: <input type="password" name="password"><br>
	<input type="submit" value="Log In">
</form>
</p>

<p>
New user?
<a href="register.php">Register Here</a>
</p>
</body>
</html>

