<html>
<head>
<title>Roost - Project Edit</title>
</head>
<body>

<?php

    // Verify user is logged in
    session_start();
    if(!isset($_SESSION['id'])) {
        die("You're not logged in!\nTry logging in <a href=\"login.php\">here</a> or make an account <a href=\"register.php\">here</a>.");
    }
    
    // Verify a project ID was passed
    if(!isset($_REQUEST['projectID'])) {
        die("No project provided!<br>Go to <a href='projects.php'>the project page</a> and click \"Edit Project\" on the project you'd like to edit here!");
    }
    
    $projectID = $_REQUEST['projectID'];
    
    $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
    $dbUsername = rtrim(fgets($cred),"\r\n");
    $dbPass = rtrim(fgets($cred),"\r\n");
    fclose($cred);
    
    // Connect to DB
    $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
    if($conn->connect_error)
        die("Database failure.");
    
    $query = "SELECT users.name,users.id,projects.projectName FROM users,projects WHERE projects.id=? AND projects.user = users.id";
    $query = $conn->prepare($query);
    $query->bind_param("i",$projectID);
    $query->execute();
    $query->bind_result($userName,$userID,$projectName);
    $query->store_result();
    
    if($query->num_rows != 1) {
        $query->close();
        $conn->close();
        die("No projects found with that ID!<br>Go to <a href='projects.php'>the project page</a> and click \"Edit Project\" on the project you'd like to edit here!");
    }
    
    $query->fetch();
    $query->close();
    
    if($userName != "GUEST" && $userID != $_SESSION['id']) {
        $conn->close();
        die($_SESSION['id'] . "<br>" . $userID . "<br>You don't have sufficient permissions to edit this project!<br>Only the user who's project this is can edit it, or all Guest/Public project can be edited!");
    }
    
    // SO BEGINS THE SAME-PAGE-REQUEST-HANDLING
    if(isset($_REQUEST['nameEdit']) && $_REQUEST['nameEdit'] == "true" && isset($_REQUEST['newName'])) {
        $query = "UPDATE projects SET projectName=? WHERE id=?";
        $query = $conn->prepare($query);
        $query->bind_param('si',$_REQUEST['newName'],$projectID);
        $query->execute();
        $query->close();
        $conn->close();
        header("Location: projectEdit.php?projectID=" . $projectID);
    }
    else if(isset($_REQUEST['massPublic']) && $_REQUEST['massPublic'] == "true") {
        $query = "UPDATE files SET public='public' WHERE projectId=?";
        $query = $conn->prepare($query);
        $query->bind_param('i',$projectID);
        $query->execute();
        $query->close();
        $conn->close();
        header("Location: projectEdit.php?projectID=" . $projectID);
    }
    else if(isset($_REQUEST['massHidden']) && $_REQUEST['massHidden'] == "true") {
        $query = "UPDATE files SET public='hidden' WHERE projectId=?";
        $query = $conn->prepare($query);
        $query->bind_param('i',$projectID);
        $query->execute();
        $query->close();
        $conn->close();
        header("Location: projectEdit.php?projectID=" . $projectID);
    }
    else if(isset($_REQUEST['massInternal']) && $_REQUEST['massInternal'] == "true") {
        $query = "UPDATE files SET public='internal' WHERE projectId=?";
        $query = $conn->prepare($query);
        $query->bind_param('i',$projectID);
        $query->execute();
        $query->close();
        $conn->close();
        header("Location: projectEdit.php?projectID=" . $projectID);
    }
    else if(isset($_REQUEST['adopt']) && $_REQUEST['adopt'] == "true") {
        $query = "UPDATE projects SET user=? WHERE id=?";
        $query = $conn->prepare($query);
        $query->bind_param('ii',$_SESSION['id'],$projectID);
        $query->execute();
        $query->close();
        $conn->close();
        header("Location: projectEdit.php?projectID=" . $projectID);
    }
    else if(isset($_REQUEST['orphan']) && $_REQUEST['orphan'] == "true") {
        $query = "UPDATE projects SET user = (SELECT id FROM users WHERE name='GUEST' AND email='not@an.email' AND password='password') WHERE id=?";
        $query = $conn->prepare($query);
        $query->bind_param('i',$projectID);
        $query->execute();
        $query->close();
        $conn->close();
        header("Location: projectEdit.php?projectID=" . $projectID);
    }
    else if(isset($_REQUEST['delete']) && $_REQUEST['delete'] == "true") {
        print("<h1>This is your last warning!</h1>\n");
        print("If you're <em>really</em> sure you want to delete \" " . $projectName . " \", type the name below.<br>\n");
        print("<form action='projectEdit.php' method='get'><input type='hidden' name='deleteConfirm' value='true' /><input type='hidden' name='projectID' value=" . $projectID . " />\n");
        print("<input type='text' name='enteredName' /><br><input type='submit' name='Delete' value='Delete' /></form>");
        $conn->close();
        die();
    }
    else if(isset($_REQUEST['deleteConfirm']) && $_REQUEST['deleteConfirm'] == 'true' && isset($_REQUEST['enteredName'])) {
        if($_REQUEST['enteredName'] == $projectName) {
            chdir('roostStorage' . DIRECTORY_SEPARATOR . $projectID);
            $zip = new ZipArchive;
            $res = $zip->open($projectID . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
            if($res === TRUE) {
                $deleteFiles = array();
                $resource = opendir('.' . DIRECTORY_SEPARATOR);
                while($fileName = readdir($resource)) {
                    $deleteFiles[] = $fileName;
                    $zip->addFile($fileName);
                }
                $zip->close();
                foreach($deleteFiles as $file) {
                    unlink($file);
                }
                $query = "UPDATE projects SET active='false' WHERE id=?";
                $query = $conn->prepare($query);
                $query->bind_param('i',$projectID);
                $query->execute();
                $query->close();
                $conn->close();
            }
        }
        header("Location: projects.php");
    }
    // SO ENDS THE SAME-PAGE-REQUEST-HANDLING
    
    print("<h1>Project Edit - " . $projectName . "</h1>\n");
    
    print("<h2>Project Utilities</h2>\n");
    print("<table border=1>\n");
    
    // Create Table Headers
    print("<tr>\n");
    print("<th><center>Edit Name</center></th>\n");
    print("<th><center>Mass File Privacy</center></th>\n");
    print("<th><center>Email Files</center></th>\n");
    print("<th><center>" . ($userID == $_SESSION['id'] ? "Orphan" : "Adopt") . " Project</center></th>\n");
    print("</tr>\n");
    
    print("<tr>\n");
        // Edit Name
        print("<td>Current Name: " . $projectName . "<br>\n");
        print("<form action='projectEdit.php' method='get'>\n");
        print("<input type=hidden name=nameEdit value=true />\n");
        print("<input type=hidden name=projectID value=" . $projectID . " />\n");
        print("New Name:<br>\n");
        print("<input type=text name=newName /><br>\n");
        print("<input type=submit name=Submit value=Submit /></td>\n");
        
        // File Privacy
        print("<td><p><center>Control the privacy of all files in this project<br>Hover on each for more info</center></p>\n");
        print("<p><center><span title='All files will be set to \"Public\".  Anyone who ever had or has a link to the file can view it.'>All Public? <a href='projectEdit.php?massPublic=true&projectID=" . $projectID . "'>Public</a></span><br>\n");
        print("<span title='All files will be set to \"Internal\".  Anyone with a valid login to Roost can use the file links to access the files.'>All Internal? <a href='projectEdit.php?massInternal=true&projectID=" . $projectID . "'>Internal</a></span><br>\n");
        print("<span title='All files will be set to \"Hidden\".  Only the project owner can access the files.  NOTE: If the project is a Guest Project, then it must be adopted before files can be accessed.'>All Hidden? <a href='projectEdit.php?massHidden=true&projectID=" . $projectID . "'>Hidden</a></center></p></td>\n");
        
        // Email Files
        print("<td>This feature currently doesn't exist.</td>\n");
        
        // Orphan/Adopt Project
        print("<td><p>This feature allows you to control who owns the project.</p>\n");
        if($userID == $_SESSION['id']) {
            print("<p>Orphaning the project will make the entire project a \"Guest\" project.<br>This is most useful for transferring projects to other users.<br>\n");
            print("<center><a href='projectEdit.php?orphan=true&projectID=" . $projectID . "'>Orphan?</a></center></p>\n");
        }
        else {
            print("<p>Adopting the project will give ownership of the entire project to you.<br>This is useful for removing projects from the Guest Project list,<br>and being able to set files to Hidden and Internal.<br>\n");
            print("<center><a href='projectEdit.php?adopt=true&projectID=" . $projectID . "'>Adopt?</a></center></p>\n");
        }
        print("</td>\n");
    print("</tr>\n");
    print("</table>\n");
    
    // File Control
    print("<h2>File Control</h2>\n");
    print("<table border=1>\n");
    print("</table>\n");
    
    // Delete Project
    print("<p><h2>Delete This Project?</h2>\n");
    print("This will make your project be deleted, and can only be recovered by a Roost administrator on your network!<br>\n");
    print("<a href='projectEdit.php?delete=true&projectID=" . $projectID . "'>Proceed?</a></p>\n");
?>
<p>
<a href="projects.php">Back to Projects List...</a>
</p>
</body>
</html>





























