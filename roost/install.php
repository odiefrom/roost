<?php

    // Yes, this is terrible for making a password, but if you're that concerned, feel free to replace this string with a far more complex and lengthy string, or, if you implement a better method for generating a random phrase, submit a pull request! <3
    $psSourcing = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $dbPass = substr(str_shuffle($psSourcing),0,10);
    
    // Where the files will be stored, along with credentials
    // It's recommended you replace the .htaccess file that will be created in this directory
    // With a Document block control in your httpd.conf file, if you have access to it.
    // If you don't, don't worry, just don't delete the .htaccess file then!
    $storageLocation = "roostStorage"; // Note, don't change this location, as Roost isn't smart enough to detect a change to it.
                                       // TODO: Make Roost able to find the changed directory?  Maybe?
    
    // This is the .htaccess file that will be put into the "roostStorage" folder to prevent access.
    // By default, these are the settings you'll want.  Some alternate ones follow, for more ideas...
    $defaultHTACCESS = "Options -Indexes\ndeny from all"; // Prevents indexing, and prevents any computer from accessing the contents of this folder through HTTP
    // $defaultHTACCESS = "Order deny,allow\ndeny from all\nall from 192.168.0.1"; // Prevents all computers except localhost from accessing folder though HTTP (change to any IP you want)
    // $defaultHTACCESS = "AuthUserFile /path/to/roost/.htpasswd\nAuthType Basic\nAuthName \"Roost Storage\"\nRequire valid-user"; // Corred the path to your .htpasswd file, and then make your htpasswd file and you can then access it with credentials

    // Change to your appropriate settings
    // Yes, you need to give your username and password for the database.
    // Roost will only use it this one time, will not store it, and will forget as soon as you delete this file!
    $servername="localhost";
    $username="username";
    $password="password";
    
    // If the database exists, it will be dropped prior to install if this is set to true.
    // This is set to false for safety reasons.  If you have a previous install, this wil need to be marked as true.
    // Check to see if there is an upgrade.php or something similar.  Use that instead if it exists.
    $dropDBIfExists = false;
    
    // The following are global Roost settings that will be stored in roost.settings in your MySQL database.
    // siteName is the name of the site that will be populated through the pages.
        // Examples would be your company name, your personal name, your organization name, etc.
    // outboundEmail is the email address that will be used to send out emails from Roost.
        // While this email doesn't need to exist, feel free to make it so that users can reply back to it directly.
    // siteAddress is the web address roost is located at.
        // This must be the complete path.
        // There must be no trailing slashes.
        // Do not include any filenames.
        // Eg., http://example.com/roost
    $siteName = "Roost";
    $outboundEmail = "roostAdmin@noreply.noexist";
    $siteAddress = "http://fake.website.reallyfake./roost";
    
    $conn= new mysqli($servername,$username,$password);
    
    if($conn->connect_error)
        die("Connection failed:\n " . $conn->connect_error);
        
    if($dropDBIfExists) {
        $query = "SET FOREIGN_KEY_CHECKS=0";
        $conn->query($query);
        $query = "DROP DATABASE IF EXISTS roost";
        $conn->query($query);
        $query = "SET FOREIGN_KEY_CHECKS=1";
        $conn->query($query);
    }
    
    $query = "CREATE DATABASE IF NOT EXISTS roost";
    if($conn->query($query) === FALSE) {
        die("Error creating database\n " . $conn->error);
    }
    
    $conn->select_db("roost");
    
    $query = "CREATE TABLE IF NOT EXISTS users (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(50) NOT NULL,
        email VARCHAR(50) NOT NULL,
        password VARCHAR(60) NOT NULL
        )";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error creating table users\n " . $conn->error);
    }
    $query->close();
    
    // Create the guest user account
    $query = "INSERT INTO users (name, email, password) VALUES ('GUEST','not@an.email','password')";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error creating GUEST user\n " . $conn->error);
    }
    $query->close();
    
    $query = "CREATE TABLE IF NOT EXISTS files (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        projectId INT(6) NOT NULL,
        fileName varchar(150) NOT NULL,
        public varchar(10) NOT NULL
        )";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error creating table files\n " . $conn->error);
    }
    $query->close();
    
    $query = "CREATE TABLE IF NOT EXISTS projects (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        user INT(6) NOT NULL,
        projectName varchar(150) NOT NULL,
        active varchar(10) NOT NULL DEFAULT 'true'
        )";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error creating table projects\n " . $conn->error);
    }
    $query->close();
    
    $query = "CREATE TABLE IF NOT EXISTS settings (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        outboundEmail varchar(150) NOT NULL,
        siteName varchar(150) NOT NULL,
        siteAddress varchar(200) NOT NULL
        )";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error creating table settings\n " . $conn->error);
    }
    $query->close();
    
    $query = "INSERT INTO settings (outboundEmail, siteName, siteAddress) VALUES ('" . $outboundEmail . "','" . $siteName . "','" . $siteAddress . "')";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error inserting global settings\n " . $conn->error);
    }
    $query->close();
    
    $query = "CREATE TABLE IF NOT EXISTS adminUsers (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        userId INT(6) NOT NULL,
        currentlyAdmin varchar(10) NOT NULL DEFAULT 'false'
        )";
    $query = $conn->prepare($query);
    if($query->execute() === FALSE) {
        die("Error creating adminUsers table!\n " . $conn->error);
    }
    $query->close();
    
    $query = "GRANT ALL PRIVILEGES ON roost.* to 'roostUser'@'localhost' IDENTIFIED BY \"" . $dbPass . "\"";
    if($conn->query($query) === FALSE) {
        die("Error granting user permissions\n " . $conn->error);
    }
    
    // For v2, this is temporary.  Eventually, guest files will properly be stored in
    //mkdir("GUEST_FILES");
    //chmod("GUEST_FILES",0755);
    
    mkdir($storageLocation);
    chmod($storageLocation,0755);
    chdir($storageLocation);
    
    $htaccess = fopen(".htaccess",w) or die("Unable to open new .htaccess file");
    fwrite($htaccess, $defaultHTACCESS);
    fclose($htaccess);
    
    // Yes, the credentials are stored in plain text, but written in a folder blocked by .htaccess
    // That said, be mindful of who has access to your server through more direct connections (FTP,SSH, etc)
    $credentials = fopen("roostCred",w) or die("Unable to open credential file!");
    fwrite($credentials,"roostUser\n");
    fwrite($credentials,$dbPass);
    fclose($credentials);
    
    $conn->close();
    
    print("Roost installed correctly!  Remove this file to be safe!");

?>