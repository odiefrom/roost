<html>
<head>
<title>Roost - Registration</title>
</head>
<body>
<?php
    if(!is_null($_REQUEST['email']) && !is_null($_REQUEST['realName']) && !is_null($_REQUEST['password'])) {
        if($_REQUEST['password'] === $_REQUEST['passwordConfirm']) {
            $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r") or die("Unable to open file");
            $dbUsername = rtrim(fgets($cred),"\r\n");
            $dbPass = rtrim(fgets($cred),"\r\n");
            fclose($cred);
            
            $password = password_hash($_REQUEST['password'], PASSWORD_BCRYPT);
            
            $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
            if($conn->connect_error)
                die("Database failure.\n" . $conn->connect_error);
            $query = "INSERT INTO users (name, email, password) VALUES (?, ?, ?)";
            $query = $conn->prepare($query);
            $query->bind_param("sss", $_REQUEST['realName'], $_REQUEST['email'], $password);
            if(!$query->execute())
                die("Database failure.\n" . $conn->error);
            $query->close();
            print("Registration Finished Successfully!<br>");
            
            // Make first user admin
            $query = "SELECT adminUsers.id FROM adminUsers";
            $query = $conn->prepare($query);
            $query->execute();
            $query->store_result();
            $numAdmins = $query->num_rows;
            $query->close();
            if($query->num_rows == 0) { // Only execute when 0 entries in adminUsers table
                $query = "SELECT id FROM users WHERE name=? AND email=? AND password=?";
                $query = $conn->prepare($query);
                $query->bind_param('sss',$_REQUEST['realName'], $_REQUEST['email'], $password);
                $query->execute();
                $query->bind_result($userID);
                $query->fetch();
                $query->close();
                
                $query = "INSERT INTO adminUsers (userId, currentlyAdmin) VALUES (?,'true')";
                $query = $conn->prepare($query);
                $query->bind_param('i', $userID);
                $query->execute();
                $query->close();
                print("Granted administrator permission by first-user rule.<br>");
            }
            
            print("Head to <a href=\"login.php\">login</a> to start using Roost!");
            $conn->close();
         }
         else {
            print("Passwords Don't Match!  Try again!");
         }
    }
?>

<p>
    <form name="register" action="register.php" method="post">
    Name: <input type="text" name="realName"><br>
    Email Address: <input type="text" name="email"><br>
    Password: <input type="password" name="password"><br>
    Confirm Password: <input type="password" name="passwordConfirm"><br>
    <input type="submit" value="Register!">
    </form>
</p>
</body>
</html>