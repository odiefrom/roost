<html>
<head>
<title>Roost - Projects</title>
</head>
<body>
<script type="text/javascript">
    function showhide(d) {
        if(document.getElementById(d).style.display == "none") { document.getElementById(d).style.display = "block"; }
        else { document.getElementById(d).style.display = "none"; }
    }
</script>

<?php
    //error_reporting(E_ALL);
    
    // Logout procedure
    if(isset($_REQUEST['logout']) && $_REQUEST['logout'] === "true") {
        session_start();
        $_SESSION = array();  // Because session_destroy doesn't destroy the session...really PHP?
        session_destroy();
        header("Location: login.php");
    }
    
    //Need to make sure the user is logged in, else the files/projects become vulnerable
    session_start();
    if(!isset($_SESSION['id'])) {
        die("You're not logged in!\nTry logging in <a href=\"login.php\">here</a> or make an account <a href=\"register.php\">here</a>.");
    }
    
    // It will create the new project, then "redirect" to this page again, without any post flags.
    // Execute if needed
    if(isset($_REQUEST['creating']) && $_REQUEST['creating'] === "true") {
        $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
        $dbUsername = rtrim(fgets($cred),"\r\n");
        $dbPass = rtrim(fgets($cred),"\r\n");
        fclose($cred);
        
        print($_REQUEST['projectName']);
        
        // Connect to DB
        $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
        if($conn->connect_error)
            die("Database failure.");
        
        // Check for name collision per user
        $query = "SELECT id FROM projects WHERE user=? and projectName=?";
        $query = $conn->prepare($query);
        $query->bind_param("is",$_SESSION['id'],$_REQUEST['projectName']);
        $query->execute();
        if($query->num_rows != 0)
            die("Must not create a project with the same name!");
        
        // Insert the new project record
        $query = "INSERT INTO projects (user,projectName) VALUES (?,?)";
        $query = $conn->prepare($query);
        $query->bind_param("is",$_SESSION['id'],$_REQUEST['projectName']);
        if(!$query->execute()) {
            $query->close();
            print("Creation of project failed!\n" . $conn->error);
            $conn->close();
            die();
        }
        else {
            $query->close();
            
            // Make the project folder
            $query = "SELECT id FROM projects WHERE user=? AND projectName=?";
            $query = $conn->prepare($query);
            $query->bind_param("is", $_SESSION['id'],$_REQUEST['projectName']);
            $query->execute();
            $query->bind_result($projectID);
            $query->fetch();
            chdir("roostStorage");
            mkdir($projectID);
            chmod($projectID, 0755);
            
            $query->close();
            $conn->close();
            header("Location: projects.php");
            exit();
        }
    }
    
    // It will upload the files, then "redirect" to this page again, without any post flags.
    else if(isset($_REQUEST['uploading']) && $_REQUEST['uploading'] === "true") {
        $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
        $dbUsername = rtrim(fgets($cred),"\r\n");
        $dbPass = rtrim(fgets($cred),"\r\n");
        fclose($cred);
        
        // Connect to DB
        $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
        if($conn->connect_error)
            die("Database failure.");
            
        $query = "SELECT id FROM projects WHERE user=? AND projectName=?";
        $query = $conn->prepare($query);
        $query->bind_param("ss",$_SESSION['id'],$_REQUEST['projectName']);
        $query->execute();
        $query->bind_result($projectID);
        $query->fetch();
        $query->close();
        
        if(!isset($projectID))
        {
            die("Project Not Found...");
            $conn->close();
        }
        
        chdir("roostStorage" . DIRECTORY_SEPARATOR . $projectID);
        
        for($count=0;$count<count($_FILES['uploadedFiles']['tmp_name']);$count++) {
            move_uploaded_file($_FILES['uploadedFiles']['tmp_name'][$count], $_FILES['uploadedFiles']['name'][$count]);
            $query = "INSERT INTO files (projectId,fileName,public) VALUES (?,?,'false')";
            $query = $conn->prepare($query);
            $query->bind_param("is",$projectID,$_FILES['uploadedFiles']['name'][$count]);
            $query->execute();
            $query->close();
        }
        $conn->close();
        header("Location: projects.php");
        exit();
    }
    
?>

<?php
    // This is for normal page operations
    
    //Need to make sure the user is logged in, else the files/projects become vulnerable
    
    print("<h1>Projects</h1>\n");
    print("<p>\n");
    print("<h2>Open Projects</h2>\n");
    
    $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
    $dbUsername = rtrim(fgets($cred),"\r\n");
    $dbPass = rtrim(fgets($cred),"\r\n");
    fclose($cred);
    
    // Connect to DB
    $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
    if($conn->connect_error)
        die("Database failure.");
    
    $subConn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
    if($subConn->connect_error) {
        die("Database failure.");
    }
    
    // Build project view
    $query = "SELECT projectName,id FROM projects WHERE user=? AND active='true'";
    $query = $conn->prepare($query);
    $query->bind_param("i", $_SESSION['id']);
    $query->execute();
    $query->bind_result($projectName,$projectID);
    print("<p><table id=\"personalFiles\" border='1'>\n");
    print("<tr><th>Personal Project Name</th><th>Files</th></tr>\n");
    while($query->fetch()) {
        print("<tr><td><center>" . $projectName . "<br><a href='projectEdit.php?projectID=" . $projectID . "'>Edit Project</a></center></td><td>\n");
        print("<a href=\"javascript:showhide('" . $projectName . "')\">Click to show/hide.</a><ul id='" . $projectName . "'>\n");
        
        $subquery = "SELECT id,fileName FROM files WHERE projectId=?";
        $subquery = $subConn->prepare($subquery);
        $subquery->bind_param("i",$projectID);
        $subquery->execute();
        $subquery->bind_result($fileID,$fileName);
        while($subquery->fetch()) {
            print("<li><a href='files.php?fileID=" . $fileID . "'>" . $fileName . "</a><br>\n");
            print("<a href='files.php?fileID=" . $fileID . "' download=" . urlencode($fileName) .">Download now</a><br></li>\n");
        }
        print("</ul></td></tr>\n");
        $subquery->close();
    }
    print("</table></p>");
    $query->close();
    
    // Stubbed out Internal Projects View
    print("<p><table id=\"internalFiles\" border='1'>\n");
    print("<tr><th>Internal Project Name</th><th>Files</th></tr>\n");
    print("</table></p>\n");
    
    // Get the guest user ID
    $query = "SELECT id FROM users WHERE name='GUEST'";
    $query = $conn->prepare($query);
    $query->execute();
    $query->bind_result($guestID);
    $query->fetch();
    $query->close();
    
    // Get all guest Projects
    $query = "SELECT projectName,id FROM projects WHERE user=? AND active='true'";
    $query = $conn->prepare($query);
    $query->bind_param("i",$guestID);
    $query->execute();
    $query->bind_result($projectName,$projectID);
    
    
    // Render the projects
    print("<p><table id=\"guestFiles\" border='1'><tr><th>Guest Project Name</th><th>Files</th></tr>\n");
    while($query->fetch()) {
        print("<tr><center><td>" . $projectName . "<br><a href='projectEdit.php?projectID=" . $projectID . "'>Edit Project</a></center></td><td>");
        print("<a href=\"javascript:showhide('" . $projectName . "')\">Click to show/hide.</a><ul id='" . $projectName . "'>");
        $subquery = "SELECT id,fileName FROM files WHERE projectId=?";
        $subquery = $subConn->prepare($subquery);
        $subquery->bind_param("i",$projectID);
        $subquery->execute();
        $subquery->bind_result($fileID,$fileName);
        while($subquery->fetch()) { // Note that public files shouldn't be made private, as then no one other than GUEST should be able to access them, violating the concept of private files
            print("<li><a href=files.php?fileID=" . $fileID . ">" . $fileName . "</a><br>");
            print("<a href=files.php?fileID=" . $fileID . " download=" . urlencode($fileName) .">Download now</a><br></li>");
        }
        print("</ul></td></tr>\n");
        $subquery->close();
    }
    print("</table></p>\n");
    
    // Build upload view
    $query = "SELECT projectName FROM projects WHERE user=? AND active='true'";
    $query = $conn->prepare($query);
    $query->bind_param("i",$_SESSION['id']);
    $query->execute();
    $query->bind_result($projectName);
    print("<h2>Upload a File</h2><p>\n");
    print("<form action=\"projects.php\" enctype=\"multipart/form-data\" method=\"post\">\n");
    print("<input type=\"hidden\" name=\"uploading\" value=\"true\">\n");
    print("Project to Upload to: ");
    print("<select name=\"projectName\">\n");
    while($query->fetch()) {
        print("<option value=\"" . $projectName . "\">" . $projectName . "</option>\n");
    }
    $query->close();
    $conn->close();
    print("</select><br>\n");
    print("File(s) to Upload: <input type=\"file\" multiple=\"multiple\" name=\"uploadedFiles[]\"><br>\n");
    print("<input type=\"submit\" value=\"Upload\">\n</form>\n</p>\n");
    
    // Build create project
    print("<p><h2>Create a New Project</h2>\n");
    print("<form action=\"projects.php\" method=\"post\">\n");
    print("Project Name: <input type=\"text\" name=\"projectName\"><br>\n");
    print("<input type=\"hidden\" name=\"creating\" value=\"true\">\n");
    print("<input type=\"submit\" value=\"Create\">\n</form>\n</p>\n");
    
    // User management, if admin
    if($_SESSION['adminID'] != -1) {
        print("<p>\n<h2>User Management</h2><a href='userManage.php'>Click here to manage users...</a></p>\n");
    }
    
    // Logout Link
    print("<p>\n<h2>Logout?</h2>\n");
    print("<a href='projects.php?logout=true'>Logout</a></p>\n");
    
?>

<script type="text/javascript">
    var rows = document.getElementById("personalFiles").rows;
    for(var i=1;i<rows.length;i++) {
        var cell = rows[i].cells[1];
        var list = cell.children[1];
        var id = list.getAttribute('id');
        showhide(id);
    }
    
    var rows = document.getElementById("guestFiles").rows;
    for(var i=1;i<rows.length;i++) {
        var cell = rows[i].cells[1];
        var list = cell.children[1];
        var id = list.getAttribute('id');
        showhide(id);
    }
</script>
</body>
</html>