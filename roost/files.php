<?php
    if(!isset($_REQUEST['fileID'])) {
        die("No file provided!<br>Return to <a href='projects.php'>the projects page</a> to select a file to view!");
    }
    
    session_start();
    
    $cred = fopen("roostStorage" . DIRECTORY_SEPARATOR . "roostCred","r");
    $dbUsername = rtrim(fgets($cred),"\r\n");
    $dbPass = rtrim(fgets($cred),"\r\n");
    fclose($cred);
    
    // Connect to DB
    $conn = new mysqli("localhost",$dbUsername,$dbPass,"roost");
    if($conn->connect_error)
        die("Database failure.");
        
    $query = "SELECT files.fileName,files.public,files.projectId,projects.user FROM files,projects WHERE files.id=? AND files.projectId=projects.id";
    $query = $conn->prepare($query);
    $query->bind_param("i",$_REQUEST['fileID']);
    $query->execute();
    $query->bind_result($fileName,$public,$projectID,$userID);
    $query->fetch();
    $query->close();
    $conn->close();
    
    // Check if the file is internal, and if so, that there is a current id in the session (ensuring that a user is logged in, we don't care /who/)
    if($public == "internal" && !isset($_SESSION['id'])) {
        die("You're not logged in!<br>Try logging in <a href=\"login.php\">here</a> or make an account <a href=\"register.php\">here</a>.");
    }
    // Check if the file is hidden, and if so, that the id in the session matches the id associated with the project.
    if($public == "hidden" && !isset($_SESSION['id']) && $_SESSION['id'] != $userID) {
        die("You're not the project owner that this file belongs to!<br>Contact the owner and ask for the privacy to be set to either \"public\" or \"internal\".<br><a href='projects.php'>Return to Projects...</a>");
    }
    // No check exists for if the file is public, since we don't care in that case.
    
    $fileDir = "roostStorage" . DIRECTORY_SEPARATOR . $projectID . DIRECTORY_SEPARATOR;
    $filePath = $fileDir . $fileName;

    if (file_exists($filePath))
    {
        $contents = file_get_contents($filePath);
        header('Content-Type: ' . mime_content_type($filePath));
        header('Content-Length: ' . filesize($filePath));
        echo $contents;
    }
    else {
        die("Error loading file...");
    }
?>